

cc.Class({
    extends: cc.Component,

    properties: {
        answerTrueAll: {
            default: null,
            type: cc.Label
        }
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.isGameOver = false;
        this.check();
        this.Answer();
    },
    Answer() {
        this.answerTrueAll.string = `Đáp án đúng là ${this.call.answerTrue4}`;
    },
    check() {
        if (this.call.answerTrue4 === 1) {
            this.call.answerTrue4 = 'A';
        } else if (this.call.answerTrue4 === 2) {
            this.call.answerTrue4 = 'B';
        } else if (this.call.answerTrue4 === 3) {
            this.call.answerTrue4 = 'C';
        } else if (this.call.answerTrue4 === 4) {
            this.call.answerTrue4 = 'D';
        }
    },
    update (dt) {
        if (!this.isGameOver) {
            this.check();
            this.isGameOver = true;
        }
    },
});
