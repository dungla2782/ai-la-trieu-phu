// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        guidePrefab: {
            default: null,
            type: cc.Prefab
        },
        audioStar1: {
            default: null,
            type: cc.AudioClip
        },
        audioStar2: {
            default: null,
            type: cc.AudioClip
        },
        audioPlayGame: {
            default: null,
            type: cc.AudioClip
        },
        button: {
            default: null,
            type: cc.Prefab
        },
    },
    start() {
        this.audio();
    },
    audio() {
        this.current1 = cc.audioEngine.play(this.audioStar1, true, 0.5);
    },
    pauseAudio() {
        this.pause = cc.audioEngine.pauseAll();
    },
    audioPlay() {
        this.audioStarGame = cc.audioEngine.play(this.audioPlayGame, false, 1);
        setTimeout(this.playGame, 4000);
    },
    playGame() {
        cc.director.loadScene('PlayGame');
        window.userData.level = 1;
        window.userData.score = 0;
    },
    Main() {
        cc.director.loadScene('Main');
        this.pauseAudio();
    },
    Guide() {
        var guide = cc.instantiate(this.guidePrefab);
        this.node.addChild(guide);
    },
    buttonVirtual: function () {
        var prefab = cc.instantiate(this.button);
        this.node.addChild(prefab);
    },

    update(dt) {

    },
});
