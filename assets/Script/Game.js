// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        showQuestions: {
            default: null,
            type: cc.Label
        },
        answerA: {
            default: null,
            type: cc.Label
        },
        answerB: {
            default: null,
            type: cc.Label
        },
        answerC: {
            default: null,
            type: cc.Label
        },
        answerD: {
            default: null,
            type: cc.Label
        },
        money: {
            default: null,
            type: cc.Label
        },
        times: {
            default: null,
            type: cc.Label
        },
        sentences: {
            default: null,
            type: cc.Label
        },
        gameOver: {
            default: null,
            type: cc.Prefab
        },
        call: {
            default: null,
            type: cc.Prefab
        },
        chartG: {
            default: null,
            type: cc.Prefab
        },
        chartG2: {
            default: null,
            type: cc.Node
        },
        // Chon A
        ansA_lossB: {
            default: null,
            type: cc.AudioClip
        },
        ansA_lossC: {
            default: null,
            type: cc.AudioClip
        },
        ansA_lossD: {
            default: null,
            type: cc.AudioClip
        },
        ansA_lossA: {
            default: null,
            type: cc.AudioClip
        },
        button: {
            default: null,
            type: cc.Prefab
        },
        // Chon B
        ansB_lossA: {
            default: null,
            type: cc.AudioClip
        },
        ansB_lossC: {
            default: null,
            type: cc.AudioClip
        },
        ansB_lossD: {
            default: null,
            type: cc.AudioClip
        },
        ansB_lossB: {
            default: null,
            type: cc.AudioClip
        },
        // Chon C
        ansC_lossA: {
            default: null,
            type: cc.AudioClip
        },
        ansC_lossB: {
            default: null,
            type: cc.AudioClip
        },
        ansC_lossD: {
            default: null,
            type: cc.AudioClip
        },
        ansC_lossC: {
            default: null,
            type: cc.AudioClip
        },
        // Chon D
        ansD_lossA: {
            default: null,
            type: cc.AudioClip
        },
        ansD_lossB: {
            default: null,
            type: cc.AudioClip
        },
        ansD_lossC: {
            default: null,
            type: cc.AudioClip
        },
        ansD_lossD: {
            default: null,
            type: cc.AudioClip
        },
        level1: {
            default: null,
            type: cc.AudioClip
        },
        level2: {
            default: null,
            type: cc.AudioClip
        },
        level3: {
            default: null,
            type: cc.AudioClip
        },
        level4: {
            default: null,
            type: cc.AudioClip
        },
        level5: {
            default: null,
            type: cc.AudioClip
        },
        level6: {
            default: null,
            type: cc.AudioClip
        },
        level7: {
            default: null,
            type: cc.AudioClip
        },
        level8: {
            default: null,
            type: cc.AudioClip
        },
        level9: {
            default: null,
            type: cc.AudioClip
        },
        level10: {
            default: null,
            type: cc.AudioClip
        },
        level11: {
            default: null,
            type: cc.AudioClip
        },
        level12: {
            default: null,
            type: cc.AudioClip
        },
        level13: {
            default: null,
            type: cc.AudioClip
        },
        level14: {
            default: null,
            type: cc.AudioClip
        },
        level15: {
            default: null,
            type: cc.AudioClip
        },
        bye: {
            default: null,
            type: cc.AudioClip
        },
        fun3: {
            default: null,
            type: cc.AudioClip
        }
    },

    onLoad: function () {
        !window.userData && (window.userData = {
            level: 1,
            score: 0
        })
        this.userLevel = window.userData.level;
        this.totalValue = window.userData.score;
        this.timer = 30;
        this.printQuestions();
        this.time();
        this.audioWinLoss();
        this.answerTrue = this.questions[this.ranQuestions][2];
        this.totalAmount = 0;
        this.isGameOver = false;
        this.answer = this.questions[this.ranQuestions][1];
        this.clearTimer = this.endTimer;
    },

    printQuestions: function () {
        this.questions = [
            ['Con chim có mấy chân?', ['1', '2', '6', '8'], 2],
            ['1 + 1 bằng mấy?', ['1', '99', '2', '4'], 3],
            ['Đâu là tên một loại mũ?', ['Lưỡi trai', 'Lưỡi bò', 'Lưỡi lợn', 'Lưỡi trâu'], 1],
            ['Mạnh vì..., bạo vì tiền', ['Cá', 'Gạo', 'Chim', 'Tiền'], 2],
            ['Thức ăn nào sau đây chứa nhiều bột đường', ['Cá', 'Thịt', 'Gạo', 'Lúa'], 3],
            ['Một chốn... quê', ['Đôi', 'Ba', 'Hai', 'Về'], 1],
            ['Ngôi chùa nào được đúc hoàn toàn bằng đồng tại Việt Nam', ['Chùa Cao', 'Chùa Đồng', 'Chùa Tây', 'Chùa Mây'], 2],
            ['Đơn vị đo dung lượng bộ nhớ nào lớn nhất', ['TB', 'GB', 'KB', 'Byte'], 1],
            ['Cây ngay không sợ...', ['Chết đứng', 'Chết nằm', 'Gãy cây', 'Giông bão'], 1],
            ['Bán buôn bán lẻ là phạm trù của ngành nào?', ['Thương nghiệp', 'Doanh nghiệp', 'Nông nghiệp', 'Công nghiệp'], 1],
            ['Hàm số nào không phải là hàm số chẵn', ['CosX.SinX', 'SinX.CosX', 'TanX', 'CosX'], 1],
            ['Người đẹp vì lụa, ... tốt vì phân', ['Hoa', 'Mây', 'Lúa', 'Cá'], 3],
            ['Sau chiến tranh thế giới 2, phong trào giải phóng dân tộc nổi lên mạnh nhất ở đâu?', ['Châu Á', 'Châu Phi', 'Châu Âu', 'Châu Mỹ'], 2],
            ['Silic là kim loại hay phi kim?', ['Á Kim', 'Á Mỹ', 'Á Tây', 'Châu Mỹ'], 1],
            ['Giải thưởng "Cù Nèo Vàng" dành cho nghệ sĩ hài được cơ quan nào trao tặng?', ['Báo mới', 'Báo tiền phong', 'Báo tuổi trẻ cười', '24H'], 3],
            ['Vua nào đặt nhiều niên hiệu nhất lịch sử nước ta', ['Lý Nhân Tông', 'Lý Thái Tổ', 'Cutin', 'Giơ ne vơ'], 1],
            ['Biển có nồng độ muối lớn nhất thế giới?', ['TBD', 'Địa Trung Hải', 'Biển Chết', 'ĐTD'], 3],

        ];

        if (window.userData.level < 3) {
            this.ranQuestions = Math.floor(0 + Math.random() * (5));
        } else {
            this.ranQuestions = Math.floor(5 + Math.random() * (11));
        }

        this.score = this.questions[this.ranQuestions][0];
        this.sentences.string = `Câu ${this.userLevel}`;
        this.showQuestions.string = this.score;
        if (this.userLevel == 1) {
            var level_1 = cc.audioEngine.play(this.level1, false, 1);
        } else if (this.userLevel == 2) {
            var level_2 = cc.audioEngine.play(this.level2, false, 1);
        } else if (this.userLevel == 3) {
            var level_3 = cc.audioEngine.play(this.level3, false, 1);
        } else if (this.userLevel == 4) {
            var level_4 = cc.audioEngine.play(this.level4, false, 1);
        } else if (this.userLevel == 5) {
            var level_5 = cc.audioEngine.play(this.level5, false, 1);
        } else if (this.userLevel == 6) {
            var level_6 = cc.audioEngine.play(this.level6, false, 1);
        } else if (this.userLevel == 7) {
            var level_7 = cc.audioEngine.play(this.level7, false, 1);
        } else if (this.userLevel == 8) {
            var level_8 = cc.audioEngine.play(this.level8, false, 1);
        } else if (this.userLevel == 9) {
            var level_9 = cc.audioEngine.play(this.level9, false, 1);
        } else if (this.userLevel == 10) {
            var level_10 = cc.audioEngine.play(this.level10, false, 1);
        } else if (this.userLevel == 11) {
            var level_11 = cc.audioEngine.play(this.level11, false, 1);
        } else if (this.userLevel == 12) {
            var level_12 = cc.audioEngine.play(this.level12, false, 1);
        } else if (this.userLevel == 13) {
            var level_13 = cc.audioEngine.play(this.level13, false, 1);
        } else if (this.userLevel == 14) {
            var level_14 = cc.audioEngine.play(this.level14, false, 1);
        } else if (this.userLevel == 15) {
            var level_15 = cc.audioEngine.play(this.level15, false, 1);
        }

        // In đáp án theo câu hỏi

        this.answerA1 = this.questions[this.ranQuestions][1][0];
        this.answerB1 = this.questions[this.ranQuestions][1][1];
        this.answerC1 = this.questions[this.ranQuestions][1][2];
        this.answerD1 = this.questions[this.ranQuestions][1][3];

        // Truyền vào đáp án

        this.answerA.string = `A: ${this.answerA1}`;
        this.answerB.string = `B: ${this.answerB1}`;
        this.answerC.string = `C: ${this.answerC1}`;
        this.answerD.string = `D: ${this.answerD1}`;

        // Truyền vào số tiền thưởng
        this.money.string = `Bonus: ${this.totalValue} $`;

    },

    checkAnsers(e, data) {
        if (this.answerTrue == data) {
            clearInterval(this.clearTimer);
            var self = this;
            this.isGameOver = true;
            var prefab = cc.instantiate(this.button);
            this.node.addChild(prefab);
            setTimeout(() => {
                window.userData.level++;
                window.userData.score += self.totalAmount;
                cc.director.loadScene('PlayGame');
                clearInterval(self.clearTimer);
            }, 15000);
        } else {
            this.playAgain();
            clearInterval(this.clearTimer);
        }
    },

    scores: function () {
        if (this.userLevel <= 3) {
            this.totalAmount = 200;
        } else if (this.userLevel <= 4) {
            this.totalAmount = 400;
        } else if (this.userLevel <= 6) {
            this.totalAmount = 1000;
        } else if (this.userLevel <= 7) {
            this.totalAmount = 3000;
        } else if (this.userLevel <= 9) {
            this.totalAmount = 4000;
        } else if (this.userLevel <= 11) {
            this.totalAmount = 8000;
        } else if (this.userLevel <= 12) {
            this.totalAmount = 10000;
        } else if (this.userLevel <= 13) {
            this.totalAmount = 20000;
        } else if (this.userLevel <= 14) {
            this.totalAmount = 25000;
        } else if (this.userLevel >= 15) {
            this.totalAmount = 65000;
        }
    },

    time: function () {
        var self = this;
        function timedCount() {
            this.timerGame = false;
            this.reversePoint = self.timer -= 1;
            self.times.string = this.reversePoint;
            if (this.reversePoint === 0) {
                self.playAgain2();
                clearInterval(self.endTimer);
                var loss = cc.audioEngine.play(this.bye, false, 3);
            }
        }
        this.endTimer = setInterval(timedCount, 1000);
    },

    // Chức năng
    exchange: function () {
        cc.director.loadScene('PlayGame');
        clearInterval(this.endTimer);
    },

    removed() {
        if (this.answerTrue === 1) {
            this.ranRemoved1 = Math.floor(2 + Math.random() * (3));
            if (this.ranRemoved1 === 2) {
                this.answerB.string = 'B: ';
                this.ranRemoved2 = Math.floor(3 + Math.random() * (2));
            } else if (this.ranRemoved1 === 3) {
                this.answerC.string = 'C: ';
                this.ranRemoved = Math.floor(0 + Math.random() * (100));
                if (this.ranRemoved < 50) {
                    this.ranRemoved2 = 2;
                } else {
                    this.ranRemoved2 = 4;
                }
            } else if (this.ranRemoved1 === 4) {
                this.answerD.string = 'D: ';
                this.ranRemoved2 = Math.floor(2 + Math.random() * (2));
            }
            if (this.ranRemoved2 === 2) {
                this.answerB.string = 'B: ';
            } else if (this.ranRemoved2 === 3) {
                this.answerC.string = 'C: ';
            } else if (this.ranRemoved2 === 4) {
                this.answerD.string = 'D: ';
            }
        } else if (this.answerTrue === 2) {
            this.ranRemoved_B = Math.floor(0 + Math.random() * (90));
            if (this.ranRemoved_B < 30) {
                this.ranRemoved1 = 1;
            } else if (this.ranRemoved_B < 60) {
                this.ranRemoved1 = 3;
            } else {
                this.ranRemoved1 = 4;
            }
            if (this.ranRemoved1 === 1) {
                this.answerA.string = 'A: ';
                this.ranRemoved2 = Math.floor(3 + Math.random() * (2));
            } else if (this.ranRemoved1 === 3) {
                this.answerC.string = 'C: ';
                this.ranRemoved_3 = Math.floor(0 + Math.random() * (10));
                if (this.ranRemoved_3 < 5) {
                    this.ranRemoved2 = 1;
                } else {
                    this.ranRemoved2 = 4;
                }
            } else if (this.ranRemoved1 === 4) {
                this.answerD.string = 'D: ';
                this.ranRemoved_4 = Math.floor(0 + Math.random() * (10));
                if (this.ranRemoved_4 < 5) {
                    this.ranRemoved2 = 1;
                } else {
                    this.ranRemoved2 = 3;
                }
            }
            // Xoá ranRemoved2
            if (this.ranRemoved2 === 1) {
                this.answerA.string = 'A: ';
            } else if (this.ranRemoved2 === 3) {
                this.answerC.string = 'C: ';
            } else if (this.ranRemoved2 === 4) {
                this.answerD.string = 'D: ';
            }
        } else if (this.answerTrue === 3) {
            this.ranRemoved_C = Math.floor(0 + Math.random() * (9));
            if (this.ranRemoved_C < 3) {
                this.ranRemoved1 = 1;
            } else if (this.ranRemoved_B < 6) {
                this.ranRemoved1 = 2;
            } else {
                this.ranRemoved1 = 4;
            }
            if (this.ranRemoved1 === 1) {
                this.answerA.string = 'A: ';
                this.ranRemoved_3A = Math.floor(0 + Math.random() * (10));
                if (this.ranRemoved_3A < 5) {
                    this.ranRemoved2 = 2;
                } else {
                    this.ranRemoved2 = 4;
                }
            } else if (this.ranRemoved1 === 2) {
                this.answerB.string = 'B: ';
                this.ranRemoved_3B = Math.floor(0 + Math.random() * (10));
                if (this.ranRemoved_3B < 5) {
                    this.ranRemoved2 = 1;
                } else {
                    this.ranRemoved2 = 4;
                }
            } else if (this.ranRemoved1 === 4) {
                this.answerD.string = 'D: ';
                this.ranRemoved2 = Math.floor(1 + Math.random() * (2));
            }
            // Xoá ranRemoved2
            if (this.ranRemoved2 === 1) {
                this.answerA.string = 'A: ';
            } else if (this.ranRemoved2 === 2) {
                this.answerB.string = 'B: ';
            } else if (this.ranRemoved2 === 4) {
                this.answerD.string = 'D: ';
            }
        } else if (this.answerTrue === 4) {
            this.ranRemoved1 = Math.floor(1 + Math.random() * (3));
            if (this.ranRemoved1 === 1) {
                this.answerA.string = 'A: ';
                this.ranRemoved2 = Math.floor(2 + Math.random() * (2));
            } else if (this.ranRemoved1 === 2) {
                this.answerB.string = 'B: ';
                this.ranRemoved_4B = Math.floor(0 + Math.random() * (10));
                if (this.ranRemoved_4B < 5) {
                    this.ranRemoved2 = 1;
                } else {
                    this.ranRemoved2 = 3;
                }
            } else if (this.ranRemoved1 === 3) {
                this.answerC.string = 'C: ';
                this.ranRemoved2 = Math.floor(1 + Math.random() * (2));
            }
            if (this.ranRemoved2 === 1) {
                this.answerA.string = 'A: ';
            } else if (this.ranRemoved2 === 2) {
                this.answerB.string = 'B: ';
            } else if (this.ranRemoved2 === 3) {
                this.answerC.string = 'C: ';
            }
        }
    },

    // Prefab
    playAgain() {
        clearInterval(this.clearTimer);
        this.node.addChild(prefab);
        setTimeout(() => {
            var prefab = cc.instantiate(self.gameOver);
            self.node.addChild(prefab);
            var loss = cc.audioEngine.play(this.bye, false, 3);
        }, 17000);
    },
    playAgain2() {
        var prefab = cc.instantiate(this.gameOver);
        this.node.addChild(prefab);
        var loss = cc.audioEngine.play(this.bye, false, 3);
    },
    prefabCall() {
        var prefab = cc.instantiate(this.call);
        this.node.addChild(prefab);
        prefab.getComponent('Call').game = this;
    },
    buttonVirtual() {
        var prefab = cc.instantiate(this.button);
        this.node.addChild(prefab);
    },
    chart() {
        var prefab = cc.instantiate(this.button);
        this.node.addChild(prefab);
        var self = this;
        setTimeout(() => {
            var prefab = cc.instantiate(self.chartG);
            self.node.addChild(prefab);
            prefab.getComponent('Chart').game = this;
        }, 6000);
    },
    audioWinLoss(e2, data2) {
        // Chọn A
        if (data2 == 1) {
            if (this.answerTrue == 2) {
                this.ansALossB = cc.audioEngine.play(this.ansA_lossB, false, 1);
            } else if (this.answerTrue == 3) {
                this.ansALossC = cc.audioEngine.play(this.ansA_lossC, false, 1);
            } else if (this.answerTrue == 4) {
                this.ansALossD = cc.audioEngine.play(this.ansA_lossD, false, 1);
            } else if (this.answerTrue == 1) {
                this.ansALossA = cc.audioEngine.play(this.ansA_lossA, false, 1);
            }
        } else if (data2 == 2) {                   // Chọn B
            if (this.answerTrue == 1) {
                this.ansBLossA = cc.audioEngine.play(this.ansB_lossA, false, 1);
            } else if (this.answerTrue == 3) {
                this.ansBLossC = cc.audioEngine.play(this.ansB_lossC, false, 1);
            } else if (this.answerTrue == 4) {
                this.ansBLossD = cc.audioEngine.play(this.ansB_lossD, false, 1);
            } else if (this.answerTrue == 2) {
                this.ansBLossB = cc.audioEngine.play(this.ansB_lossB, false, 1);
            }
        } else if (data2 == 3) {                   // Chọn C
            if (this.answerTrue == 1) {
                this.ansCLossA = cc.audioEngine.play(this.ansC_lossA, false, 1);
            } else if (this.answerTrue == 2) {
                this.ansCLossB = cc.audioEngine.play(this.ansC_lossB, false, 1);
            } else if (this.answerTrue == 4) {
                this.ansCLossD = cc.audioEngine.play(this.ansC_lossD, false, 1);
            } else if (this.answerTrue == 3) {
                this.ansCLossC = cc.audioEngine.play(this.ansC_lossC, false, 1);
            }
        } else if (data2 == 4) {                   // Chọn D
            if (this.answerTrue == 1) {
                this.ansDLossA = cc.audioEngine.play(this.ansD_lossA, false, 1);
            } else if (this.answerTrue == 2) {
                this.ansDLossB = cc.audioEngine.play(this.ansD_lossB, false, 1);
            } else if (this.answerTrue == 3) {
                this.ansDLossC = cc.audioEngine.play(this.ansD_lossC, false, 1);
            } else if (this.answerTrue == 4) {
                this.ansDLossD = cc.audioEngine.play(this.ansD_lossD, false, 1);
            }
        }
    },
    audioCn3: function () {
        this.cn3 = cc.audioEngine.play(this.fun3, false, 1);
    },
    update(dt) {
        if (!this.isGameOver) {
            this.scores();
            this.isGameOver = true;
        }
        if (!this.isGameOver) {
            this.checkAnsers();
            this.isGameOver = true;
        }
    },
});