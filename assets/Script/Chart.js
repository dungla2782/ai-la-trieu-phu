
cc.Class({
    extends: cc.Component,

    properties: {
        speed: 10,
        answerA: {
            default: null,
            type: cc.Label
        },
        answerB: {
            default: null,
            type: cc.Label
        },
        answerC: {
            default: null,
            type: cc.Label
        },
        answerD: {
            default: null,
            type: cc.Label
        },
        barA: {
            default: null,
            type: cc.ProgressBar
        },
        barB: {
            default: null,
            type: cc.ProgressBar
        },
        barC: {
            default: null,
            type: cc.ProgressBar
        },
        barD: {
            default: null,
            type: cc.ProgressBar
        },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.isGameOver = false;
        this.checkAnswer();
        this.load = true;
        this.barA.progress = 0;
        this.barB.progress = 0;
        this.barC.progress = 0;
        this.barD.progress = 0;
    },
    checkAnswer() {
        if (this.game.answerTrue === 1) {
            this.score = Math.floor(40 + Math.random() * (80 - 40));
            this.answerA.string = `${this.score}%`;
            this.falseB = Math.floor(1 + Math.random() * ((100 - this.score) - 1));
            this.answerB.string = `${this.falseB}%`;
            this.falseC = Math.floor(1 + Math.random() * ((100 - (this.score + this.falseB)) - 1));
            this.answerC.string = `${this.falseC}%`;
            this.falseD = 100 - (this.score + this.falseB + this.falseC);
            this.answerD.string = `${this.falseD}%`;
        } else if (this.game.answerTrue === 2) {
            this.score = Math.floor(40 + Math.random() * (80 - 40));
            this.answerB.string = `${this.score}%`;
            this.falseA = Math.floor(1 + Math.random() * ((100 - this.score) - 1));
            this.answerA.string = `${this.falseA}%`;
            this.falseC = Math.floor(1 + Math.random() * ((100 - (this.score + this.falseA)) - 1));
            this.answerC.string = `${this.falseC}%`;
            this.falseD = 100 - (this.score + this.falseA + this.falseC);
            this.answerD.string = `${this.falseD}%`;
        } else if (this.game.answerTrue === 3) {
            this.score = Math.floor(40 + Math.random() * (80 - 40));
            this.answerC.string = `${this.score}%`;
            this.falseA = Math.floor(1 + Math.random() * ((100 - this.score) - 1));
            this.answerA.string = `${this.falseA}%`;
            this.falseB = Math.floor(1 + Math.random() * ((100 - (this.score + this.falseA)) - 1));
            this.answerB.string = `${this.falseB}%`;
            this.falseD = 100 - (this.score + this.falseA + this.falseB);
            this.answerD.string = `${this.falseD}%`;
        } else if (this.game.answerTrue === 4) {
            this.score = Math.floor(40 + Math.random() * (80 - 40));
            this.answerD.string = `${this.score}%`;
            this.falseA = Math.floor(1 + Math.random() * ((100 - this.score) - 1));
            this.answerA.string = `${this.falseA}%`;
            this.falseB = Math.floor(1 + Math.random() * ((100 - (this.score + this.falseA)) - 1));
            this.answerB.string = `${this.falseB}%`;
            this.falseC = 100 - (this.score + this.falseA + this.falseB);
            this.answerC.string = `${this.falseC}%`;
        }
    },
    destroyChart() {
        this.node.destroy();
    },
    update (dt) {
        if (!this.isGameOver) {
            this.checkAnswer();
            this.isGameOver = true;
        }
        this.updateProgressBarA(this.barA, dt);
        this.updateProgressBarB(this.barB, dt);
        this.updateProgressBarC(this.barC, dt);
        this.updateProgressBarD(this.barD, dt);
    },
    updateProgressBarA: function (progressBar, dt) {
        var progress = progressBar.progress;
        if (this.game.answerTrue === 1) {
            this.long = this.score / 100;
        } else {
            this.long = this.falseA / 100;
        }
        if (progress < this.long && this.load) {
            progress += (dt * this.speed) * 10;
        }
        progressBar.progress = progress;
    },
    updateProgressBarB: function (progressBar, dt) {
        var progress = progressBar.progress; 
        this.long = this.score / 100;
        if (this.game.answerTrue === 2) {
            this.long = this.score / 100;
        } else {
            this.long = this.falseB / 100;
        }  
        if (progress < this.long && this.load) {
            progress += (dt * this.speed) * 10;
        }
        progressBar.progress = progress;
    },
    updateProgressBarC: function (progressBar, dt) {
        var progress = progressBar.progress; 
        this.long = this.score / 100;     
        if (this.game.answerTrue === 3) {
            this.long = this.score / 100;
        } else {
            this.long = this.falseC / 100;
        }
        if (progress < this.long && this.load) {
            progress += (dt * this.speed) * 10;
        }
        progressBar.progress = progress;
    },
    updateProgressBarD: function (progressBar, dt) {
        var progress = progressBar.progress;   
        this.long = this.score / 100;   
        if (this.game.answerTrue === 4) {
            this.long = this.score / 100;
        } else {
            this.long = this.falseD / 100;
        }
        if (progress < this.long && this.load) {
            progress += (dt * this.speed) * 10;
        }
        progressBar.progress = progress;
    },
});
