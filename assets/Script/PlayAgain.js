

cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        
    },
    
    playAgain: function () {
        cc.director.loadScene('PlayGame');
        window.userData.level = 1;
        window.userData.score = 0;
    },
    quitGame: function () {
        cc.director.loadScene('Main');
    }
    // update (dt) {},
});
